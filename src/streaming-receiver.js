import { EventHubClient, EventData, EventPosition, MessagingError, ReceiveOptions } from '@azure/event-hubs'
import assert from 'assert'

const debug = require('debug')('azure-streaming-receiver')

const DEFAULT_BATCH_MAX_READ_MESSAGES = 100 // How many messages to read from the EventHub at one time
const DEFAULT_BATCH_MAX_READ_DELAY = 10 // Number of seconds to wait for the above number of messages
const DEFAULT_LARGE_DELAY = 2000
const DEFAULT_MAX_MESSAGES_BUFFERED = 1000
/**
 * The streaming-receiver is responsible reading Messsage from an Azure EventHub.
 *
 * The functionality provided
 * - reads from a specific event hub server and Event Hub (see 'config.eventHub.connectionString, config.eventHub.name')
 * - reads as a specific consumer (see 'config.eventHub.consumerGroup')
 * - reads in batches see BATCH_MAX_READ_MESSAGES
 */
export const factory = (options) => {
    let { config, logger } = options

    let isStreamStopped = false
    let client
    let buffered = []
    let whenToReceive = 0

    const MAX_MESSAGES_BUFFERED = config.eventHub.maxMessagesBuffered ? config.eventHub.maxMessagesBuffered : DEFAULT_MAX_MESSAGES_BUFFERED
    const LARGE_DELAY = config.eventHub.largedelay ? config.eventHub.largedelay : DEFAULT_LARGE_DELAY

    const BATCH_MAX_READ_MESSAGES = config.eventHub.maxReadMessages ? config.eventHub.maxReadMessages : DEFAULT_BATCH_MAX_READ_MESSAGES
    const BATCH_MAX_READ_DELAY = config.eventHub.maxReadDelay ? config.eventHub.maxReadDelay : DEFAULT_BATCH_MAX_READ_DELAY // Number of seconds to wait for the above number of messages

    async function onMessage (eventData) {
        try {
            debug('Received event.')
            const body = eventData.body

            if (body instanceof Object) {
                buffered.push(eventData)
            } else {
                if (logger) { logger.warn(`Skipped empty or non-conforming object: ${JSON.stringify(body)}`) }
            }
            if (buffered.length > MAX_MESSAGES_BUFFERED) {
                if (logger) { logger.error('To many unpublished messages. Please, resolve issue and restart') }
                stop()
            }
        } catch (err) {
            logger.error(err)
        }
    }

    function onError (err) {
        logger.error(`On receive data: ${err}`)
    }

    async function * handleBuffered (interval = 200) {
        while (true) {
            if (isStreamStopped) {
                debug('Ending Generator')
                return
            }
            if (buffered.length > 0) {
                let next = buffered.shift()
                debug(`Yielding with ${JSON.stringify(next.body)}`)
                yield next.body
            } else {
                await new Promise((resolve) => {
                    setTimeout(resolve, 1000)
                })
            }
        }
    }

    function receiveFrom (configInitialOffset) {
        let intialCheckpoint = Date.now()
        if (configInitialOffset) {
            const offsetMilliseconds = 1000 * Number.parseInt(configInitialOffset, 10)
            intialCheckpoint = intialCheckpoint - offsetMilliseconds
        }
        const d = new Date(intialCheckpoint)
        debug(`Reading from ${d}`)
        return intialCheckpoint
    }

    function stop () {
        isStreamStopped = true
        buffered = []
        if (client && client.close) {
            return client.close()
        }
    }

      /**
     * This function determines the next offset to read from in the read from event hub loop.
     * Whenever a batch is read we want to continue reading from an offset one greater highest offset in the batch
     * @param batch
     */
    function getNextSequence (batch) {
        return (batch[batch.length - 1].sequenceNumber) + 1
    }

    async function consume () {
        const wtr = whenToReceive

        debug(`Consuming as consumer group: ${config.eventHub.consumerGroup}`)
        const options = {
            eventPosition: EventPosition.fromEnqueuedTime(wtr),
            // enableReceiverRuntimeMetric: true,
            prefetchCount: 1
            // consumerGroup: config.eventHub.consumerGroup
        }

        debug(`Receiving from: ${wtr}`)

        while (true) {
            if (isStreamStopped) {
                debug('Ending consumption')
                return
            }
            try {
                debug('\tReceiveBatch')
                const batch = await client.receiveBatch('0' /* partition */, BATCH_MAX_READ_MESSAGES /* max messages */, BATCH_MAX_READ_DELAY /* max delay */, options)

                for (let i = 0; i < batch.length; i++) {
                    const eventData = batch[i]
                    debug(`Enqueue Time: ${eventData.enqueuedTimeUtc}`)
                    onMessage(eventData)
                }
                if (batch.length > 0) {
                    const t = EventPosition
                    const nextSequence = getNextSequence(batch)
                    let next = EventPosition.fromSequenceNumber(nextSequence)
                    debug(`Sequence Number now ${nextSequence}`)
                    options.eventPosition = next
                }
                if (buffered.length > MAX_MESSAGES_BUFFERED) {
                    debug('To many unpublished messages, backing of batch read')
                    await new Promise(async (resolve) => {
                        while (true) {
                            await new Promise((resolve) => {
                                setTimeout(resolve, LARGE_DELAY)
                            })
                            if (buffered.length < 10) {
                                resolve()
                            }
                        }
                    })
                }
            } catch (err) {
                let e = err
                if (!e.message.startsWith('The supplied sequence number')) {
                    if (logger) { logger.warn(e) }
                }
            }
        }
    }

    function createClient () {
        whenToReceive = receiveFrom(config.eventHub.initialReadOffset)
        debug(`Consuming from connection: ${config.eventHub.connectionString}`)
        debug(`Consuming from name: ${config.eventHub.name}`)
        return EventHubClient.createFromConnectionString(config.eventHub.connectionString, config.eventHub.name)
    }

    function start (theclient) {
        assert(theclient, 'Client must not be null')
        client = theclient
        if (logger) { logger.info('Receiving now') }
        consume(whenToReceive)
        return handleBuffered()// a generator is returned
    }

    return {
        createClient,
        start,
        stop
    }
}
