const input = 'index.js'
const sourcemap = true

export default [{
    input,
    output: {
        file: 'dist/azure-eventhub-streaming-receiver.mjs',
        format: 'es',
        sourcemap
    }
}, {
    input,
    output: {
        file: 'dist/azure-eventhub-streaming-receiver.js',
        format: 'cjs',
        sourcemap
    }
},
{
    input,
    output: {
        file: 'dist/azure-eventhub-streaming-receiver.umd.js',
        format: 'umd',
        name: 'azure-eventhub-streaming-receiver',
        sourcemap
    }
}]
