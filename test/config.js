import assert from 'assert'
const debug = require('debug')('lib')

export const Config = () => {
    assert(process.env['EVENT_HUB_CONNECTION_STRING'], 'EVENT_HUB_CONNECTION_STRING must be defined')

    const connectionString = process.env['EVENT_HUB_CONNECTION_STRING']
    const topic = process.env['EVENT_HUB_NAME']
    const initialReadOffset = process.env['INITIAL_OFFSET'] || 0 // This is a value in units of seconds
    const consumerGroup = process.env.EVENT_HUB_CONSUMER_GROUP

    return {
        eventHub: {
            connectionString,
            name: topic,
            consumerGroup,
            initialReadOffset
        }
    }
}
