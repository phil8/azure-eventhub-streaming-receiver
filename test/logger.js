
import Debug from 'debug'
let debug = Debug('logger')

export const logger = {
    info: (msg) => {
        debug('[INFO]:', msg)
    },
    warn: (msg) => {
        debug('[WARN]:', msg)
    },
    error: (msg) => {
        debug('[ERROR]:', msg)
    },
    debug: (msg) => {
        debug('[DEBUG]:', msg)
    }
}
