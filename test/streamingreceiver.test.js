import { describe } from 'riteway'

import receiverFactory from '../index'

import DotEnv from 'dotenv'

import { Config } from './config.js'
import { logger } from './logger.js'

DotEnv.config({ path: 'variables.test.env' })

const debug = require('debug')('test')

let config = null

let integrationTest = false
if (!integrationTest) {
  config = {
    eventHub: {
        connectionString: '',
        name: 'A topic',
        consumerGroup: 'A consumer group',
        initialReadOffset: 0,
        maxMessagesBuffered: 10,
        maxReadMessages: 2,
        maxReadDelay: 0
    }
  }
} else {
  config = Config() // for an integration test
}

debug(config)

let mockClient = {
  close: function () {
  },
  receiveBatch: function () {
    return [{ body: { 'foo': 'bar' }, sequenceNumber: 1 }, { body: { 'foo2': 'bar2' }, sequenceNumber: 2 }]
  }
}

describe('Streaming Receiver', async (assert) => {
  let receiverControl = await receiverFactory({ config, logger })

  let client = null
  if (!integrationTest) {
    client = mockClient
  } else {
    client = await receiverControl.createClient()
  }
  let generator = receiverControl.start(client)

    let i = 0
    for await (let value of generator) {
      i = i + 1
      debug(value)
      if (i >= 2) {
        break
      }
    }
    receiverControl.stop()

    assert({
        given: 'a ',
        should: 'be',
        actual: '1',
        expected: '1'
      })
})
