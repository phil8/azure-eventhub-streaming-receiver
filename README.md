# Azure Event Hub Streaming Receiver    

The Azure Event Hub Streaming Receiver is a library which efficiently reads messages from an Azure Event Hub (Kafka).

The Azure Event Hub nodejs client is easily misused and overloads the process which is reading from the Azure Event Hub.
This library provides a client which can help effectively read from Azure Event Hub queues.

This library 
- reads from the Azure Event Hub in configurable batch sizes
- with a configurable timeout
- and throttles reads while previously read message are not successfully processed.

![](./images/StreamingReceiver.png)


## Background

Azure Event Hub is based on apache kafka and is a different paradigm from traditional brokers.  There is a good descriptiton of the difference [here](https://content.pivotal.io/blog/understanding-when-to-use-rabbitmq-or-apache-kafka)

Kafka

![](./images/kafka-overview.png)

RabbitMQ

![](./images/rabbitmq-overview.png)

## How to use

Install the module
```
npm install azure-eventhub-streaming-receiver
or
yarn add azure-eventhub-streaming-receiver
```

Import or Require the module
```
import receiverFactory from 'azure-eventhub-streaming-receiver'
or
let receiverFactory = require('azure-eventhub-streaming-receiver')
```

Prepare a config object
```
let config = {
        eventHub: {
            connectionString: '',
            name: 'the topic',
            consumerGroup: 'the consumer group',
            initialReadOffset: 'the initial read offset',
            maxMessagesBuffered: 1000,
            maxReadMessages: 100,
            maxReadDelay: 1000
        }
```

Create a receiver, start it and iterate indefinitely over the values from the generator returned.
```
let receiver = receiverFactory({config})
let client = createClient()
let generator = receiver.start()
for await (let value of generator) {
    //the values
}
```

## How to setup

Gets dependencies
```
npm install
or
yarn
```

## How to run

```
npm run start
or
yarn 
```
This will lint the source, build the library,  run the tests and watch the file system for any changes.




